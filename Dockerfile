FROM debian:latest
MAINTAINER tom@jorquera.net

RUN apt update && apt --yes install git virtualenv python-pip wget unzip postgresql-client

RUN git clone https://git.laquadrature.net/memopol/memopol.git && \
    cd memopol && \
    cp src/memopol/local_settings.py.example src/memopol/local_settings.py && \
    sed -i 's/localhost/db/' src/memopol/local_settings.py

WORKDIR memopol

CMD bash -c './bin/dev.sh && \
             echo yes | ./bin/quickstart.sh && \
             source memopol_env/bin/activate && \
             export DJANGO_DEBUG=True DJANGO_SETTINGS_MODULE=memopol.settings && \
             bin/update_all ; \
             echo "Finished!"'