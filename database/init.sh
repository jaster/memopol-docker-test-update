#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --command="create user memopol with password 'memopol';"
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --command="alter role memopol with createdb;"
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --command="create database memopol with owner memopol;"
